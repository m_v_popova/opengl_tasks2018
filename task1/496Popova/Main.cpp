#define _USE_MATH_DEFINES

#include "TorusSurface.h"

int main()
{
	TorusSurface app;
	app.start();
	return 0;
}