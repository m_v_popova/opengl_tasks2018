set(SRC_FILES
	common/Application.cpp
        
	common/Camera.cpp
        
	common/Mesh.cpp
        
	common/ShaderProgram.cpp
        
	common/Application.hpp
 
	common/DebugOutput.cpp       
	common/Camera.hpp
        
	common/Mesh.hpp
        
	common/ShaderProgram.hpp
              
	Main.cpp
)


MAKE_TASK(496Popova 1 "${SRC_FILES}")

target_include_directories(496Popova1 PUBLIC
        common)