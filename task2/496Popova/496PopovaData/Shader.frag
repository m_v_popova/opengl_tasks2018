#version 330

uniform sampler2D textureSampler;

in vec2 texCoord;

out vec4 fragColor;

void main()
{
	fragColor = vec4( texture(textureSampler, texCoord).rgb, 1.0);
}
