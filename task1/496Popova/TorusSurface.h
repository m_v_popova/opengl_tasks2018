#include "Application.hpp"
#include <cmath>
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include <iostream>
#include <algorithm>

class TorusSurface : public Application
{
private:

	glm::vec3 countVertex(double u, double v) {
		double x = (aa + bb * cos(u))*cos(v);
		double y = (aa + bb * cos(u))*sin(v);
		double z = bb * sin(u);

		return glm::vec3({ x, y, z });
	}

	glm::vec3 countNormal(double u, double v) {
		double x_dev_u = -bb * sin(u)*cos(v);
		double x_dev_v = -(aa + bb * cos(u))*sin(v);
		double y_dev_u = -bb * sin(u)*sin(v);
		double y_dev_v = (aa + bb * cos(u))*cos(v);
		double z_dev_u = bb * cos(u);
		double z_dev_v = 0;

		glm::vec3 dev_u = glm::vec3(x_dev_u, y_dev_u, z_dev_u);
		glm::vec3 dev_v = glm::vec3(x_dev_v, y_dev_v, z_dev_v);
		return glm::normalize(glm::cross(dev_u, dev_v));
	}

	MeshPtr surface;
	ShaderProgramPtr shader;

	float delta_u;
	float delta_v;
	float aa;
	float bb;
	float u_from;
	float u_to;
	float v_from;
	float v_to;

public:

    TorusSurface() {
		this->delta_u = 0.1;
		this->delta_v = 0.1;
		this->aa = 2.0;
		this->bb = 1.0;
		this->u_from = 0;
		this->u_to = 2 * glm::pi<float>();
		this->v_from = 0;
		this->v_to = 2 * glm::pi<float>();
	}

    void makeScene() override {
		Application::makeScene();

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;

		for (double u = u_from; u < u_to; u += delta_u) {
			for (double v = v_from; v < v_to; v += delta_v) {
				double next_v_triangle = v + delta_v > v_to ? v_to : v + delta_v;
				double next_u_triangle = u + delta_u > u_to ? u_to : u + delta_u;
				glm::vec3 vertex1 = countVertex(u, v);
				glm::vec3 vertex2 = countVertex(u, next_v_triangle);
				glm::vec3 vertex3 = countVertex(next_u_triangle, v);
				glm::vec3 vertex4 = countVertex(next_u_triangle, next_v_triangle);

				glm::vec3 normal1 = countNormal(u, v);
				glm::vec3 normal2 = countNormal(u, next_v_triangle);
				glm::vec3 normal3 = countNormal(next_u_triangle, v);
				glm::vec3 normal4 = countNormal(next_u_triangle, next_v_triangle);

				vertices.push_back(vertex1);
				vertices.push_back(vertex2);
				vertices.push_back(vertex3);
				vertices.push_back(vertex2);
				vertices.push_back(vertex3);
				vertices.push_back(vertex4);

				normals.push_back(normal1);
				normals.push_back(normal2);
				normals.push_back(normal3);
				normals.push_back(normal2);
				normals.push_back(normal3);
				normals.push_back(normal4);
			}
		}

		DataBufferPtr bufVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		bufVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

		DataBufferPtr bufNormals = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		bufNormals->setData(normals.size() * sizeof(float) * 3, normals.data());

		surface = std::make_shared<Mesh>();
		surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufVertices);
		surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, bufNormals);

		surface->setPrimitiveType(GL_TRIANGLES);
		surface->setVertexCount(vertices.size());

		std::cout << "Surface is created with " << vertices.size() << " vertices\n";

		shader = std::make_shared<ShaderProgram>();
		shader->createProgram("./496PopovaData/shaderNormal.vert", "./496PopovaData/shader.frag");
	}

    void update() override { Application::update(); }

    void draw() override {
		Application::draw();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//������������� ������
		shader->use();

		//������������� ����� �������-����������
		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		shader->setMat3Uniform("normalToCameraMatrix",
			glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * surface->modelMatrix()))));

		//������ ���
		shader->setMat4Uniform("modelMatrix", surface->modelMatrix());
		surface->draw();

	}

    void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);

		const double multiplier = 0.5;

		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_MINUS) {
				delta_u *= (1.0 / multiplier);
				delta_v *= (1.0 / multiplier);
				makeScene();
			}
			if (key == GLFW_KEY_EQUAL) {
				delta_u *= multiplier;
				delta_v *= multiplier;
				makeScene();
			}
		}
	}
};
