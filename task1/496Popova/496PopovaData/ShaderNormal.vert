#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;
out vec3 vertexNormalRespectToCamera; //������� � ������� ��������� ������
out vec4 vertexPositionRespectToCamera; //���������� ������� � ������� ��������� �����

void main()
{
    vertexPositionRespectToCamera = viewMatrix * modelMatrix * vec4( vertexPosition, 1.0 );
    vertexNormalRespectToCamera = normalize( normalToCameraMatrix * vertexNormal );

    if( dot( vertexNormalRespectToCamera, vertexPositionRespectToCamera.xyz ) < 0) {
        vertexNormalRespectToCamera *= -1.0;
    }
	/*
	��� �� ����� �������� ���� (�������� [0; 1]), �� ����� ������� ������� (�������� [-1; 1]).
	������� ����� ��������� � ���� ��� ���������� � ���������������� �����.
	*/
	color.rgb = vertexNormalRespectToCamera.xyz * 0.5 + 0.5;

	gl_Position = projectionMatrix * vertexPositionRespectToCamera;
}
